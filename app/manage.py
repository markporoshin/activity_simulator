import sys
import logging
import traceback

from src.generate.models.sysadmin import SysAdmin

logging.basicConfig(
    level=int(logging.INFO),
    format="[%(asctime)s][%(process)d][%(thread)d]:" + logging.BASIC_FORMAT,
    handlers=[
        logging.FileHandler('activity_simulator.log')
    ]
)

log = logging.getLogger(__name__)

if __name__ == '__main__':
    try:
        mode = input_file = sys.argv[1]
        if mode == 'sysadmin':
            print('start sysadmin mode', flush=True)
            sysadmin = SysAdmin('localhost', 'asap', 'Qwerty7')
            sysadmin.run()
        elif mode == 'attacker':
            print('start attacker mode', flush=True)
    except:
        traceback_msg = str(traceback.format_exc())
        log.exception(traceback_msg)