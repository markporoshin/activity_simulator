from src.generate.models.user import User
import random
import logging

log = logging.getLogger(__name__)

class Attacker(User):

    def __init__(self, host, user, psw):
        super().__init__(host, user, psw)
        self.actions = [self.copy_many_files, self.download_many_files]
        self.actions_probs = [100, 1]
        self.actions_names = ['copy_many_files', 'download_many_files']
        self.freq = 1 / (60 * 60 * 4)  # event per 4 hour

    def copy_many_files(self):
        number_of_execution = random.randint(50, 100)
        log.info(f'copy {number_of_execution} files')
        for _ in range(number_of_execution):
            dst = 'face@10.0.0.19:/home/face/Documents/dont_touch.txt'
            src = f'/home/face/Documents/buffer/__{self.rw.generate()}.scp'
            command = f"scp {dst} {src}"
            self.ssh.execute(command)

    def download_many_files(self):
        number_of_execution = random.randint(50, 100)
        log.info(f'download {number_of_execution} files')
        for _ in range(number_of_execution):
            command = f"wget -O /home/face/Documents/buffer/{self.rw.generate()}.wget www.examplesite.com/textfile.txt"
            self.ssh.execute(command)


if __name__ == '__main__':
    attacker = Attacker('localhost', 'asap', 'Qwerty7')
    attacker.run()
    # wget -O /home/face/Documents/buffer/test.wget www.examplesite.com/textfile.txt