from RandomWordGenerator import RandomWord
from src.generate.ssh import Ssh
from math import fabs
import numpy as np
from time import sleep
import random
import logging
import traceback

log = logging.getLogger(__name__)


class User:

    def __init__(self, host, user, psw):
        self.host = host
        self.user = user
        self.psw = psw
        self.actions = []
        self.actions_probs = []
        self.actions_names = []
        self.freq = 0
        self.get_series = lambda : 1
        self.rw = RandomWord(10,
                constant_word_size=True,
                include_digits=False,
                special_chars=r"@_!#$%^&*()<>?/\|}{~:",
                include_special_chars=False)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ssh.__exit__(exc_type, exc_val, exc_tb)

    def run(self):
        choices = []
        for i, el in enumerate(self.actions_probs):
            choices += [i] * el
        while True:
            try:
                self.ssh = Ssh(self.host, self.user, self.psw)
                series = self.get_series()
                log.info(f'--------\nstart actions serial({series})')
                for act_num in [random.choice(choices) for _ in range(series)]:
                    log.info(f'action={self.actions_names[act_num]}')
                    self.actions[act_num]()
                sleep_time = fabs(np.random.normal(size=(1, 1))[0, 0] / self.freq)
                log.info(f'end actions serial({series}); sleep=[{sleep_time // 60 // 60}h, {sleep_time // 60 % 60}m, {sleep_time % 60 % 60}s]')
                log.handlers[0].flush()
                sleep(sleep_time)
                self.ssh.close()
            except:
                traceback_msg = str(traceback.format_exc())
                log.exception(traceback_msg)
                