from src.generate.models.user import User
import datetime
import random
from math import fabs
import logging

log = logging.getLogger(__name__)


class SysAdmin(User):

    def __init__(self, host, user, psw):
        super().__init__(host, user, psw)
        self.actions = [
            self.insert_tests_db,
            self.select_tests_db,
            self.del_user,
            self.add_user,
            self.create_file,
            self.del_file,
            self.ping_google,
            self.edit_conf
        ]
        self.actions_probs = [
            1, 1, 1, 1, 1, 1, 1, 1
        ]
        self.actions_names = [
            'insert_tests_db',
            'select_tests_db',
            'del_user',
            'add_user()',
            'created_files',
            'del_file',
            'ping_google',
            'edit_conf'
        ]
        self.get_series = lambda : int(fabs(random.gauss(0, 5)))
        self.freq = 0.1
        self.created_users = []
        self.created_files = []
        self.file_limit = 20
        self.user_limit = 20

    def insert_tests_db(self):
        query = f"insert into test (info) values ('{self.rw.generate()}')"
        command = f'sudo -u postgres psql -d diploma -c "{query}"'
        anws = self.ssh.execute(command)
        log.info(command + "\t\t" + str(anws))

    def select_tests_db(self):
        query = "select * from test"
        command = f'sudo -u postgres psql -d diploma -c "select * from test"'
        anws = self.ssh.execute(command)
        log.info(command + "\t\t" + str(anws))

    def del_user(self):
        if not self.created_users:
            return
        username = random.choice(self.created_users)
        command = f"sudo userdel {username}"
        self.created_users.remove(username)
        anws = self.ssh.execute(command)
        log.info(command + "\t\t" + str(anws))

    def add_user(self):
        log.info(f"created_users: {str(self.created_users)}")
        if len(self.created_users) > self.user_limit:
            return
        user = self.rw.generate()
        command = f"sudo adduser {user}"
        anws = self.ssh.execute(command)
        self.created_users.append(user)
        log.info(command + "\t\t" + str(anws))

    def ping_google(self):
        command = f"ping -c 5 www.google.com"
        answ = self.ssh.execute(command)
        log.info(command + "\t\t" + str(answ))

    def edit_conf(self):
        conf_files = [
            "/etc/bash.bashrc",
            "/etc/env",
            "/etc/env",
            "/etc/hostname"
        ]
        file = random.choice(conf_files)
        command = f"sudo echo '#new commend by sysadmin {datetime.datetime.today().strftime('%Y-%m-%d-%H:%M:%S')}' >> {file}"
        answ = self.ssh.execute(command)
        log.info(command + "\t\t" + str(answ))

    def del_file(self):
        if not self.created_files:
            return
        file = random.choice(self.created_files)
        command = f"rm {file}"
        answ = self.ssh.execute(command)
        self.created_files.remove(file)
        log.info(command + "\t\t" + str(answ))

    def create_file(self):
        log.info(f"created_files: {str(self.created_files)}")
        if len(self.created_files) > self.file_limit:
            return
        start_path = '/home/face/Documents/'
        path = self.rand_dir(start_path)
        if '/home/face' not in path:
            log.info("cannot choose dir")
            return
        file = path + self.rw.generate() + '.txt'
        command = f'touch {file}'
        self.created_files.append(file)
        answ = self.ssh.execute(command)
        log.info(command + "\t\t" + str(answ))
        command = f"echo '#new commend by sysadmin {datetime.datetime.today().strftime('%Y-%m-%d-%H:%M:%S')}' >> {file}"
        answ = self.ssh.execute(command)
        log.info(command + "\t\t" + str(answ))

    def rand_dir(self, path):
        for _ in range(random.randint(0, 100)):
            command = f'ls -d {path}*/ | shuf -n1'
            new_path = (self.ssh.execute(command) + [path])[0]
            if 'ls: cannot access' in new_path:
                return path
            path = new_path
            log.info(command + "\t\t" + path)
        return path


if __name__ == '__main__':
    sysadmin = SysAdmin('localhost', 'asap', 'Qwerty7')
    sysadmin.run()
